# Android Studio Coursework

This is the Android Studio Coursework that I was originally going to submit as my piece of work.
Unfortunately, since someone else was already doing something pretty similar and the time constraints
made completing this work to the best of my ability not possible, I decided to give up with this
project and work on an alternative project, which I did end up submitting in the end.